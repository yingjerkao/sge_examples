# sge_examples

This repo consists of some simple SGE scripts for job submission. 

To submit jobs, use `qsub` with the `run_XXX` script files.

For example, to run a serial job, 

`qsub run_serial`

and use 

`qstat` to monitor how your jobs are runnin


To check what queues are avilable, use

`qconf -sql`

To look at the current load on the nodes, use

`qstat -f`
