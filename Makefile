mpiCC=mpicc
mpiCXX=mpiCC
CC=gcc
CXX=g++

pi.e: pi.c
	$(mpiCC) -o $@ $^


.phony: clean
clean:
	rm *.e
